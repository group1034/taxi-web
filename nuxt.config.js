const webpack = require('webpack')

module.exports = {
  modules: [
    //'@nuxtjs/bootstrap-vue',
    '@nuxtjs/font-awesome'
  ],
  
  head: {
    titleTemplate: 'Taxi',//'%s - Nuxt.js',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' }
      //{ name: 'viewport', content: 'width=device-width, initial-scale=1' },
      //{ hid: 'description', name: 'description', content: 'Meta description' }
    ]
  },
  
  css: [
    //'../node_modules/bootstrap/dist/css/bootstrap.css',
    //'../node_modules/bootstrap-vue/dist/bootstrap-vue.css',
    'bootstrap/dist/css/bootstrap.css',
    //'bootstrap-vue/dist/bootstrap-vue.css'
    '~/css/style.css',
    '~/css/style.scss',
    //'bootstrap/dist/css/bootstrap.css',
  ],
  plugins: [
    //'~plugins/bootstrap.js'
    //'~/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js'
  ],
  /*plugins: [
    '~/node_modules/jquery/dist/jquery.js',
    '~/node_modules/tether/dist/js/tether.min.js',
    '~/node_modules/bootstrap/dist/js/bootstrap.min.js'
  ],*/
  build: {
    vendor: [
      'axios', 
      'jquery', 
      'tether', 
      'popper.js',
      'bootstrap',
      //'vue-i18n'
    ],
    plugins: [
      // set shortcuts as global for bootstrap
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
        Tether: 'tether',
        "window.Tether": 'tether',
        Popper: 'popper.js',
        "window.Popper": 'popper.js',
      })
      
    ],
    extractCSS: true
  },
  router: {
    scrollBehavior: function (to, from, savedPosition) {
      return { x: 0, y: 0 }
    },
    //middleware: 'i18n'
  },
  /*
  plugins: ['~/plugins/i18n.js',],
  generate: {
    routes: [ '/th', '/en']
  }
  */
  /*
  router: {
    middleware: 'i18n'
  },
  plugins: ['~/plugins/i18n.js',],
  generate: {
    routes: ['/', '/about', '/en', '/en/about']
  }
  */
  /*
  render: {
    bundleRenderer: {
      shouldPreload: (file, type) => {
        return ['script', 'style', 'font'].includes(type)
      }
    }
  },
  */
  
}